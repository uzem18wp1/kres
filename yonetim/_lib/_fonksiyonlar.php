<?php
function tr_ay($ingilizceay = null)
{
    if (is_null($ingilizceay)) {
        $ingilizceay = date('f');
    }

    $donustur = array(
        'january'	=> 'ocak',
        'february'	=> 'şubat',
        'march'		=> 'mart',
        'april'		=> 'nisan',
        'may'		=> 'mayıs',
        'june'		=> 'haziran',
        'july'		=> 'temmuz',
        'august'	=> 'ağustos',
        'september'	=> 'eylül',
        'october'	=> 'ekim',
        'november'	=> 'kasım',
        'december'	=> 'aralık',
    );

    foreach ($donustur as $ingilizceanahtar => $turkcedeger) {
        if ($ingilizceay === $ingilizceanahtar) {
            return $turkcedeger;
        }
    }

    return 'tanınmayan ay';
}

function tr_gun($ingilizcegun = null)
{
    if (is_null($ingilizcegun)) {
        $ingilizcegun = date('l');
    }

    $donustur = array(
        'monday'    => 'pazartesi',
        'tuesday'   => 'salı',
        'wednesday' => 'çarşamba',
        'thursday'  => 'perşembe',
        'friday'    => 'cuma',
        'saturday'  => 'cumartesi',
        'sunday'    => 'pazar',
    );

    foreach ($donustur as $ingilizceanahtar => $turkcedeger) {
        if ($ingilizcegun === $ingilizceanahtar) {
            return $turkcedeger;
        }
    }

    return 'tanınmayan gün';
}

function tr_date($bicim, $zamanmetni = 'now')
{
    $zaman = date("$bicim", strtotime($zamanmetni));

    $ingilizceturkceeslesme = array(
        'monday'    => 'pazartesi',
        'tuesday'   => 'salı',
        'wednesday' => 'çarşamba',
        'thursday'  => 'perşembe',
        'friday'    => 'cuma',
        'saturday'  => 'cumartesi',
        'sunday'    => 'pazar',
        'january'   => 'ocak',
        'february'  => 'şubat',
        'march'     => 'mart',
        'april'     => 'nisan',
        'may'       => 'mayıs',
        'june'      => 'haziran',
        'july'      => 'temmuz',
        'august'    => 'ağustos',
        'september' => 'eylül',
        'october'   => 'ekim',
        'november'  => 'kasım',
        'december'  => 'aralık',
        'mon'       => 'pts',
        'tue'       => 'sal',
        'wed'       => 'çar',
        'thu'       => 'per',
        'fri'       => 'cum',
        'sat'       => 'cts',
        'sun'       => 'paz',
        'jan'       => 'oca',
        'feb'       => 'şub',
        'mar'       => 'mar',
        'apr'       => 'nis',
        'jun'       => 'haz',
        'jul'       => 'tem',
        'aug'       => 'ağu',
        'sep'       => 'eyl',
        'oct'       => 'eki',
        'nov'       => 'kas',
        'dec'       => 'ara',
    );

    foreach ($ingilizceturkceeslesme as $ingilizcead => $turkcead) {
        $zaman = str_replace($ingilizcead, $turkcead, $zaman);
    }

    if (strpos($zaman, 'mayıs') !== false && strpos($bicim, 'f') === false) {
        $zaman = str_replace('mayıs', 'may', $zaman);
    }

    return $zaman;
}

function tr_strtolower($text)
{
    $search=array("ç","i̇","i","ğ","ö","ş","ü");
    $replace=array("ç","i","ı","ğ","ö","ş","ü");
    $text=str_replace($search,$replace,$text);
    $text=strtolower($text);
    return $text;
}

function tr_strtoupper($text)
{
    $search=array("ç","i","ı","ğ","ö","ş","ü");
    $replace=array("ç","i̇","i","ğ","ö","ş","ü");
    $text=str_replace($search,$replace,$text);
    $text=strtoupper($text);
    return $text;
}

function tr_ucfirst($str) {
    $tmp = preg_split("//u", $str, 2, preg_split_no_empty);
    return mb_convert_case(
            str_replace("i", "i̇", $tmp[0]), mb_case_title, "utf-8").
        $tmp[1];
}

function sifredogrulama($sifre)
{
    $hatalar = [];

    if (strlen($sifre) < 8) {
        $hatalar[] = 'şifre çok kısa.';
    }

    if (strlen($sifre) > 20) {
        $hatalar[] = 'şifre çok uzun.';
    }

    if (!preg_match('#[0-9]+#', $sifre)) {
        $hatalar[] = 'şifre en az bir rakam içermeli.';
    }

    if (!preg_match('#[a-z]+#', $sifre)) {
        $hatalar[] = 'şifre en az bir harf içermeli.';
    }

    if (!preg_match('#[a-z]+#', $sifre)) {
        $hatalar[] = 'şifre en az bir büyük harf içermeli.';
    }

    if (!preg_match('#\w+#', $sifre)) {
        $hatalar[] = 'şifre en az bir özel karakter içermeli.';
    }

    if (count($hatalar) === 0) {
        return true;
    } else {
        return $hatalar;
    }
}