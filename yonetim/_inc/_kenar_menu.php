<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center"><img src="img/avatar-1.jpg" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">Anderson Hardy</h2><span>
              Sistem Yöneticisi
              </span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Ana Menü</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li><a href="index.php"> <i class="icon-home"></i>Anasayfa                             </a></li>
                <li><a href="veli_liste.php"> <i class="icon-form"></i>Veliler                             </a></li>
                <li><a href="tables.html"> <i class="icon-grid"></i>Öğrenciler                             </a></li>
                <li><a href="tables.html"> <i class="icon-grid"></i>Öğretmenler                             </a></li>
                <li><a href="tables.html"> <i class="icon-grid"></i>Pedagoglar                             </a></li>
                <li><a href="tables.html"> <i class="icon-grid"></i>Mesaj Kutusu (6)                             </a></li>
            </ul>
        </div>
        <div class="admin-menu">
            <h5 class="sidenav-heading">Sistem Menüsü</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                <li> <a href="#"> <i class="icon-screen"> </i>Kullanıcılar</a></li>
                <li> <a href="#"> <i class="icon-screen"> </i>SMS Ayarları</a></li>
                <li> <a href="#"> <i class="icon-screen"> </i>Email Ayarları</a></li>
            </ul>
        </div>
    </div>
</nav>