<?php
require_once '_ayarlar.php';
?>
    <!DOCTYPE html>
    <html>
<head>
    <?php
    include_once '_inc/_head_ust.php';
    ?>
    <title>
        <?= KURUM . ' | ' . PROJE_ADI; ?></title>
    <?php
    include_once '_inc/_head_alt.php';
    ?>
</head>
<body>
<!-- Side Navbar -->
<?php
include_once '_inc/_kenar_menu.php';
?>

<div class="page">
    <!-- navbar-->
    <?php
    include_once '_inc/_header.php';
    ?>

    <section class="section-padding">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Öğrenci Güncelle</h4>
                        </div>
                        <div class="card-body">

                            <form>
                                <div class="form-group">
                                    <label>Ad</label>
                                    <input type="text" placeholder="Öğrenci Ad" value="Mehmet" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Soyad</label>
                                    <input type="text" placeholder="Öğrenci Soyad" value="Yılmaz" class="form-control" required>
                                </div>


                                <div class="form-group">
                                    <label>Doğum Tarihi</label>
                                    <input type="text" placeholder="Öğrenci Eposta" value="20/07/2014" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Sınıf</label>
                                    <input type="text" placeholder="Sınıf" value="A-1" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Öğrenci Velisi</label>
                                    <input type="text" placeholder="Öğrenci Velisi" value="Ahmet Yılmaz" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <input type="submit" value="Kaydet" class="btn btn-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php
    include_once '_inc/_footer.php';
    ?>
</div>

<?php
include_once '_inc/_body_alt.php';
?>
</body>
    </html><?php
/**
 * Created by PhpStorm.
 * User: MONSTER
 * Date: 24.6.2018
 * Time: 15:54
 */