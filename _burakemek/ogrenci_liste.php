<?php
require_once '_ayarlar.php';
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '_inc/_head_ust.php';
    ?>
    <title>
        <?= KURUM . ' | ' . PROJE_ADI; ?></title>
    <?php
    include_once '_inc/_head_alt.php';
    ?>
</head>
<body>
<!-- Side Navbar -->
<?php
include_once '_inc/_kenar_menu.php';
?>

<div class="page">
    <!-- navbar-->
    <?php
    include_once '_inc/_header.php';
    ?>

    <section class="section-padding">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-header">
                            <h4>Öğrenci Listesi <a class="btn btn-success btn-sm" href="ogrenci_ekle.php"> <i class="fa fa-plus"></i> Yeni Kayıt</a> </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ad</th>
                                        <th>Soyad</th>
                                        <th>Doğum Tarihi</th>
                                        <th>Sınıf</th>
                                        <th>Veli</th>
                                        <th>İşlem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mehmet</td>
                                        <td>Yılmaz</td>
                                        <td>20/07/2014</td>
                                        <td>A-1</td>
                                        <td>Ahmet Yılmaz</td>
                                        <td>
                                            <a href="ogrenci_detay.php">Detay</a> |
                                            <a href="ogrenci_guncelle.php">Güncelle</a> |
                                            <a href="ogrenci_sil.php">Sil</a> |
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <?php
    include_once '_inc/_footer.php';
    ?>
</div>

<?php
include_once '_inc/_body_alt.php';
?>
</body>
</html>