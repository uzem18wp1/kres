<?php
require_once '_ayarlar.php';
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '_inc/_head_ust.php';
    ?>
    <title>
        <?= KURUM . ' | ' . PROJE_ADI; ?></title>
    <?php
    include_once '_inc/_head_alt.php';
    ?>
</head>
<body>
<!-- Side Navbar -->
<?php
include_once '_inc/_kenar_menu.php';
?>

<div class="page">
    <!-- navbar-->
    <?php
    include_once '_inc/_header.php';
    ?>

    <section class="section-padding">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-header">
                            <h4>Öğretmen Detayları </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <tbody>
                                    <tr>
                                        <th>#</th>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <th>Ad</th>
                                        <td>Hasan</td>
                                    </tr>
                                    <tr>
                                        <th>Soyad</th>
                                        <td>Tokatlı</td>
                                    </tr>

                                    <tr>
                                        <th>Sınıf</th>
                                        <td>A-1</td>
                                    </tr>
                                    <tr>
                                        <th>Telefon</th>
                                        <td>05551234567</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <a class="btn btn-info" href="ogretmen_guncelle.php">Güncelle</a>
                            <a class="btn btn-danger" href="ogretmen_sil.php">Sil</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <?php
    include_once '_inc/_footer.php';
    ?>
</div>

<?php
include_once '_inc/_body_alt.php';
?>
</body>
</html>