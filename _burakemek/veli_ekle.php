<?php
require_once '_ayarlar.php';
?>
<!DOCTYPE html>
<html>
  <head>
      <?php
      include_once '_inc/_head_ust.php';
      ?>
        <title>
            <?= KURUM . ' | ' . PROJE_ADI; ?></title>
      <?php
      include_once '_inc/_head_alt.php';
      ?>
  </head>
  <body>
    <!-- Side Navbar -->
    <?php
    include_once '_inc/_kenar_menu.php';
    ?>

    <div class="page">
      <!-- navbar-->
        <?php
        include_once '_inc/_header.php';
        ?>

      <section class="section-padding">
        <div class="container-fluid">
          <div class="row">

              <div class="col-lg-12">

                  <div class="card">
                      <div class="card-header d-flex align-items-center">
                          <h4>Veli Ekle</h4>
                      </div>
                      <div class="card-body">

                          <form>
                              <div class="form-group">
                                  <label>Ad</label>
                                  <input type="text" placeholder="Veli Ad" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <label>Soyad</label>
                                  <input type="text" placeholder="Veli Soyad" class="form-control" required>
                              </div>


                              <div class="form-group">
                                  <label>Eposta</label>
                                  <input type="email" placeholder="Veli Eposta" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <label>Telefon</label>
                                  <input type="text" placeholder="Veli telefon" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <input type="submit" value="Kaydet" class="btn btn-primary">
                              </div>
                          </form>
                      </div>
                  </div>
              </div>

          </div>
        </div>
      </section>

       <?php
            include_once '_inc/_footer.php';
        ?>
    </div>

    <?php
    include_once '_inc/_body_alt.php';
    ?>
  </body>
</html>