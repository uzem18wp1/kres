<?php
require_once '_ayarlar.php';
?>
<!DOCTYPE html>
<html>
  <head>
      <?php
      include_once '_inc/_head_ust.php';
      ?>
        <title>
            <?= KURUM . ' | ' . PROJE_ADI; ?></title>
      <?php
      include_once '_inc/_head_alt.php';
      ?>
  </head>
  <body>
    <!-- Side Navbar -->
    <?php
    include_once '_inc/_kenar_menu.php';
    ?>

    <div class="page">
      <!-- navbar-->
        <?php
        include_once '_inc/_header.php';
        ?>

      <section class="section-padding">
        <div class="container-fluid">
          <div class="row">

              <div class="col-lg-12">

                  <div class="card">
                      <div class="card-header d-flex align-items-center">
                          <h4>Veli Güncelle</h4>
                      </div>
                      <div class="card-body">

                          <form>
                              <div class="form-group">
                                  <label>Ad</label>
                                  <input type="text" placeholder="Veli Ad" value="Ahmet" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <label>Soyad</label>
                                  <input type="text" placeholder="Veli Soyad" value="Yılmaz" class="form-control" required>
                              </div>


                              <div class="form-group">
                                  <label>Eposta</label>
                                  <input type="email" placeholder="Veli Eposta" value="ahmetyilmaz@gmail.com" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <label>Telefon</label>
                                  <input type="text" placeholder="Veli telefon" value="536 654 85 65" class="form-control" required>
                              </div>

                              <div class="form-group">
                                  <input type="submit" value="Kaydet" class="btn btn-primary">
                              </div>
                          </form>
                      </div>
                  </div>
              </div>

          </div>
        </div>
      </section>

       <?php
            include_once '_inc/_footer.php';
        ?>
    </div>

    <?php
    include_once '_inc/_body_alt.php';
    ?>
  </body>
</html>